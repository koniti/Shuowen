-- example
create table kaiji_title (
  chapter varchar(10) NOT NULL COMMENT '篇ID',
  title   text                 COMMENT '篇title',
  PRIMARY KEY (`chapter`)
)
 COMMENT='說文解字注_篇定義'
 DEFAULT CHARSET=utf8mb4;

create table kaiji_chu (
  id  varchar(10) NOT NULL COMMENT '說文解字注ID',
  bun text                 COMMENT '文章',
  PRIMARY KEY (`id`)
)
 COMMENT='說文解字注_中身'
 DEFAULT CHARSET=utf8mb4;

create table kaiji_moji (
  chapter varchar(10) NOT NULL COMMENT 'title.chapter',
  id      varchar(10) NOT NULL COMMENT 'chu.id',
  img     varchar(10) COMMENT 'image file name',
  c       varchar(10) COMMENT '対象の字',
  parts   varchar(10) COMMENT '文字構造',
  PRIMARY KEY (`id`)
)
 COMMENT='說文解字注_字と本文の結びつき'
 DEFAULT CHARSET=utf8mb4;

/*
create table kaiji_base (
  id  varchar(10) COMMENT '',
  bun text        COMMENT '',
  PRIMARY KEY (`id`)
)
 COMMENT='說文解字注_'
 DEFAULT CHARSET=utf8mb4;
*/
